﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;


namespace ARM2
{
    public partial class Form1 : Form
    {
        bool vis = true;
        bool start = true;
        bool conn = false;

        bool isBusy = false;
        bool isConnected = false;
        bool isQempty = true;
        Queue<Location> q=new Queue<ARM2.Location>();

     
        ViewModel vm = new ViewModel();

        public Form1()
        {
            InitializeComponent();
            Change();
           


        }
        //control buttons
        private void YUp_Click(object sender, EventArgs e)
        {
            vm.IncreaseY();
        }
        private void YDown_Click(object sender, EventArgs e)
        {
            vm.DecreaseY();
        }
        private void XRight_Click(object sender, EventArgs e)
        {
            vm.IncreaseX();
        }
        private void XLeft_Click(object sender, EventArgs e)
        {
            vm.DecreaseX();
        }
        private void BaseClockwise_Click(object sender, EventArgs e)
        {
            vm.IncreaseAlpha();
        }
        private void BaseCclockwise_Click(object sender, EventArgs e)
        {
            vm.DecreaseAlpha();
        }
        private void AngleUp_Click(object sender, EventArgs e)
        {
            vm.IncreaseAngle();
        }
        private void AngleDown_Click(object sender, EventArgs e)
        {
            vm.DecreaseAngle();
        }
        private void RotClockwise_Click(object sender, EventArgs e)
        {

            vm.IncreaseRot();
        }
        private void RotCclockwise_Click(object sender, EventArgs e)
        {
            vm.DecreaseRot();
        }
        private void Grab_Scroll(object sender, EventArgs e)
        {
            vm.setGrabVal(Grab.Value);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            string output = "";
            foreach (Location a in vm.coords)
            {
                output += a.name + " " + a.alpha + " " + a.beta + " " + a.gamma + " " + a.delta + " " + a.rot + " " + a.grab + Environment.NewLine;
            }
            File.WriteAllText("coords.txt", output);
            output = vm.makra.Count.ToString()+Environment.NewLine;
            foreach (macro m in vm.makra)
            {
                output += m.name+" "+m.Count;
                foreach (var l in m)
                {
                    output += Environment.NewLine + l.name + " " + l.alpha + " " + l.beta + " " + l.gamma + " " + l.delta + " " + l.rot + " " + l.grab;
                }
                output += Environment.NewLine;
            }
            File.WriteAllText("macros.txt", output);

        }

        private void Form1_Layout(object sender, LayoutEventArgs e)
        {
            if (start)
            {

                string input = "";
                if (!File.Exists("coords.txt"))
                    File.Create("coords.txt");
                if (!File.Exists("macros.txt"))
                    File.WriteAllText("macros.txt","0");
                input = File.ReadAllText("coords.txt");
                string[] input1 = input.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string[] input2 = new string[7];
                foreach (string a in input1)
                {
                    input2 = a.Split(' ');
                    vm.coords.Add(new Location(input2[0], Int32.Parse(input2[1]), Int32.Parse(input2[2]), Int32.Parse(input2[3]), Int32.Parse(input2[4]), Int32.Parse(input2[5]), Int32.Parse(input2[6])));
                }
                string output = "";
                foreach (Location a in vm.coords)
                {
                    output += a.ToString() + Environment.NewLine;
                }
                start = false;
                foreach (Location a in vm.coords)
                {
                    vm.allLocationsList.Add(a.name);
                   // UpdateListBox();
                    //Change();
                }
                LocationList.Items.AddRange(vm.allLocationsList.ToArray());
                panel1.BackColor = Color.Red;
                vm.tmp.edit("temporary", vm.alpha, Math.Round(vm.beta * 180 / Math.PI, 0), Math.Round(vm.gamma * 180 / Math.PI, 0), Math.Round(vm.delta * 180 / Math.PI, 0), vm.rot, vm.grab);
                input = File.ReadAllText("macros.txt");
                input1 = input.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                macro m;
                int i = Convert.ToInt32(input1[0]); //iterator makr
                int j = 1;  //iterator linii
                int k;      //iterator lokacji w makrze
                while (i > 0)
                {
                    input2 = input1[j].Split(' ');
                    m = new macro(input2[0]);
                    k = Convert.ToInt32(input2[1]);
                    j++;
                    while(k>0)
                    {
                        input2 = input1[j].Split(' ');
                        m.Add(new Location(input2[0], Int32.Parse(input2[1]), Int32.Parse(input2[2]), Int32.Parse(input2[3]), Int32.Parse(input2[4]), Int32.Parse(input2[5]), Int32.Parse(input2[6])));
                        k--;
                        j++;
                    }
                    i--;
                    vm.makra.Add(m);
                    
                }
                foreach(var m2 in vm.makra)
                {
                    vm.macroList.Add(m2.name);
                }
                vm.OnChangeMade += Change;
                // vm.OnCheckFalse += () => { MessageBox.Show("It's Physically impossible"); };
                vm.OnAllLocationsListChange += () =>
                {
                    LocationList.Items.Clear();
                    LocationList.Items.AddRange(vm.allLocationsList.ToArray());
                };
                vm.OnSelectedLocationsListChange += () =>
                {
                    MacroLocationsList.Items.Clear();
                    MacroLocationsList.Items.AddRange(vm.selectedLocationsList.ToArray());
                };
                vm.OnMacroListChange += () =>
                {
                    MacroList.Items.Clear();
                    MacroList.Items.AddRange(vm.macroList.ToArray());
                };
                vm.OnMsgShow += (string message) => { MessageBox.Show(message); };
                vm.Refresh();
            }
        }

        private void Change()
        {
            
                send(vm.tmp);
        
        }
        
        private void Connection(object sender, EventArgs e)
        {
            COMList.Items.Clear();
            foreach (string a in SerialPort.GetPortNames())
            {
                COMList.Items.Add(a);
            }
            COMList.Visible = vis;
            Connect.Visible = vis;
            vis = !vis;
        }
        
        private void AddLoc_Click(object sender, EventArgs e)
        {
                if (String.IsNullOrEmpty(LocName.Text))
                    MessageBox.Show("Enter location name");
                else if (LocationList.Items.Contains(LocName.Text))
                    MessageBox.Show("Location name must be unique");
                else
                {
                vm.AddToAllLocationsList(LocName.Text);
                      }
            
        }

        private void AddLocToMacro_Click(object sender, EventArgs e)
        {
                try
                {
                vm.AddToSelectedLocationsList(LocationList.SelectedItem.ToString());
                }
                catch
                {
                    MessageBox.Show("No location selected!");
                }
            
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            
                if (!String.IsNullOrEmpty(LocationList.Text)) vm.AddToSelectedLocationsList(LocationList.Text);
        }

        private void DelLocFromMacro_Click(object sender, EventArgs e)
        {
            vm.RemoveFromSelectedLocationsList(MacroLocationsList.SelectedIndex);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            
                if (String.IsNullOrEmpty(MacroName.Text))
                    MessageBox.Show("Enter macro's name");
                else
                {
                    if (MacroList.Items.Contains(MacroName.Text))
                        MessageBox.Show("Macro's name must be unique");
                    else
                    {
                    vm.AddMacro(MacroName.Text);
                    }
                
            }
        }

        private void delLocButton_Click(object sender, EventArgs e)
        {
            
                try
                {
                vm.RemoveLocationFromAllLocationsList(LocationList.Text);
                }
                catch
                {
                    MessageBox.Show("No location selected!");
                }
            
        }

        private void Abort_Click(object sender, EventArgs e)
        {
            
                isBusy = false;
                panel1.BackColor = Color.Green;
        }
        
        private void delMacroBut_Click(object sender, EventArgs e)
        {
            
                try
                {
                vm.DeleteMacro(MacroList.Text);
                }
                catch
                {
                    MessageBox.Show("No macro selected!");
                }
            
        }

        private void ConnectButton(object sender, EventArgs e)
        {
            try
            {

                if (!conn)
                {
                    serialPort1.PortName = COMList.SelectedItem.ToString();
                    serialPort1.Open();
                    conn = true;
                }
                serialPort1.Write("3");
            }
            catch
            {
                MessageBox.Show("Output error");
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int input = Int32.Parse(serialPort1.ReadLine());
            if (input == 2)
            {
                
                isConnected = true;
                panel1.BackColor = Color.Green;
            }
            if( input == 3)
            {
                isBusy = false;
                panel1.BackColor = Color.Green;
                if(q.Count>0)
                {
                    var a = q.Dequeue();

                    send(a);
                    MessageBox.Show(a.ToString());

                }
                else
                {
                    isQempty = false;
                }
            }
            
        }

        private void send(Location loc)
        {
            if(!isBusy && isConnected)
            {
                serialPort1.Write(loc.ToString());
                isBusy = true;
                panel1.BackColor = Color.Yellow;
            }
        }

        private void executeMacroButton_Click(object sender, EventArgs e)
        {
            try
            {
                isQempty = true;
                foreach (var l in vm.makra[MacroList.SelectedIndex])
                {
                    q.Enqueue(l);
                }
                var a = q.Dequeue();

                send(a);
                MessageBox.Show(a.ToString());
            }
            catch
            {

            }
        }

        private void save_Click(object sender, EventArgs e)
        {
           
            vm.R1 = double.Parse(first.Text);
            vm.R2 = double.Parse(second.Text);
            vm.R3 = double.Parse(third.Text);
            vm.speed = double.Parse(speed.Text);
            vm.angspeed = double.Parse(angspeed.Text);
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                vm.selectedLocationsList=vm.makra[MacroList.SelectedIndex].GetLocationList();
                MacroLocationsList.Items.Clear();
                MacroLocationsList.Items.AddRange(vm.selectedLocationsList.ToArray());
            }
            catch { }
        }

        private void sendCurrent_Click(object sender, EventArgs e)
        {
            send(vm.tmp);
        }
    }
}
