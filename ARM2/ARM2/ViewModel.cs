﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ARM2
{
    delegate void OnChangeMadeEvent();
    delegate void OnAllLocationsListChangeEvent();
    delegate void OnSelectedLocationsListChangeEvent();
    delegate void OnMacroListChangeEvent();
    delegate void OnMsgShowEvent(string message);

    class ViewModel
    {
        // these constants will be changed, depending on the final lengths after assembly
        public  double R1 = 180; // length of the first member
        public  double R2 = 120; // length of the second member
        public  double R3 = 130; // length of the third member
        // value of incrementation of the coords;
        public double speed = 10;
        public double angspeed = 2;
        // coordinates given by user via interface
        // basic values will change depending on starting position of the arm
        public double x = 120;
        public double y = 50;

        public double angle = 90;
        public double angleRad;

        public double alpha;
        //variables describing the grabber
        public double rot;
        public double grab;
        //angles from inverse kinematics
        public double beta; // 1st joint
        public double gamma; // 2nd joint
        public double delta; // 3rd joint

        // some random variables
        public double tmp1, tmp2, tmp3, tmp4;
        public double checkBeta, checkGamma, checkDelta;
        public bool vis = true;
        public bool start = true;
        public bool conn = false;

        public bool isBusy = false;
        public bool isConnected = false;

        public event OnChangeMadeEvent OnChangeMade;    
        public event OnAllLocationsListChangeEvent OnAllLocationsListChange;
        public event OnSelectedLocationsListChangeEvent OnSelectedLocationsListChange;
        public event OnMacroListChangeEvent OnMacroListChange;
        public event OnMsgShowEvent OnMsgShow;          //error output from program (different implementations for different platforms)

        public List<String> allLocationsList=new List<string>(), selectedLocationsList=new List<string>(), macroList=new List<string>();
        public Location tmp = new Location("", 0, 0, 0, 0, 0, 0);
        public List<Location> coords = new List<Location>();
        public List<macro> makra = new List<macro>();


        public void IncreaseY()
        {
            y += speed;
            if (checker())
            {
                Change();
            }
            else
            {
                y -= speed;
            }


        }
        public  void DecreaseX()
        {


            x -= speed;
            if (checker())
            {
                Change();

            }
            else
            {
                x += speed;
            }

        }
        public void DecreaseY()
        {


            y -= speed;
            if (checker())
            {
                Change();
            }
            else
            {
                y += speed;
            }

        }
        public void IncreaseX()
        {


            x += speed;
            if (checker())
            {
                Change();
            }
            else
            {
                x -= speed;
            }

        }
        public void IncreaseAlpha()
        {


            alpha += angspeed;
            if (checker())
            {
                Change();
            }
            else
            {
                alpha -= speed;
            }

        }
        public void DecreaseAlpha()
        {


            alpha -= angspeed;
            if (checker())
            {
                Change();
            }
            else
            {
                alpha += speed;
            }

        }
        public void IncreaseAngle()
        {


            angle += angspeed;
            if (checker())
            {
                Change();
            }
            else
            {
                angle -= speed;
            }

        }
        public void DecreaseAngle()
        {


            angle -= angspeed;
            if (checker())
            {
                Change();
            }
            else
            {
                angle += speed;
            }

        }
        public void IncreaseRot()
        {
            rot += angspeed;
            if (rot > 180)
            {
                OnMsgShow?.Invoke("It is physically impossible!");
                rot -= angspeed;
            }
            Change();

        }
        public void DecreaseRot()
        {


            rot -= angspeed;
            if (rot < 0)
            {
                OnMsgShow?.Invoke("It is physically impossible!");
                rot += angspeed;
            }
            Change();

        }
        public void setGrabVal(double val)
        {
            grab = val;
            Change();
        }
        //calculations for inverse kinematics
        public double skrotX(double kat, double dl, double x)
        {
            return x - dl * Math.Cos(kat);
        }
        public double skrotY(double kat, double dl, double y)
        {
            return y - dl * Math.Sin(kat);
        }
        public double cosBeta(double x1, double y1, double l1, double l2)
        {
            return (Math.Pow(x1, 2) + Math.Pow(y1, 2) - Math.Pow(l1, 2) - Math.Pow(l2, 2)) / (2 * l1 * l2);
        }
        public double sinBeta(double cosBeta)
        {
            return -Math.Sqrt(Math.Abs(1 - Math.Pow(cosBeta, 2)));
        }
        public double a(double x, double y, double k1, double k2)
        {
            return Math.Atan2(y, x) - Math.Atan2(k2, k1);
        }
        public double b(double sinBeta, double cosBeta)
        {
            return Math.Atan2(sinBeta, cosBeta);
        }
        public double c(double a, double b, double A)
        {
            return Math.PI - (a + b) + A;
        }
        private void Change()
        {
            angleRad = angle * Math.PI / 180 - Math.PI;
            tmp1 = skrotX(angleRad, R3, x);
            tmp2 = skrotY(angleRad, R3, y);
            tmp3 = cosBeta(tmp1, tmp2, R1, R2);
            tmp4 = sinBeta(tmp3);
            
                 
        beta = a(tmp1, tmp2, R1 + R2 * tmp3, R2 * tmp4);
            gamma = b(tmp4, tmp3);
            delta = c(beta, gamma, angleRad);
            
            tmp.edit("temporary", alpha, Math.Round(beta * 180 / Math.PI, 0), Math.Round(gamma * 180 / Math.PI, 0), Math.Round(delta * 180 / Math.PI, 0), rot, grab);
            //send(tmp);
            OnChangeMade?.Invoke();
        }    
        bool checker()
        {
            angleRad = angle * Math.PI / 180 - Math.PI;
            tmp1 = skrotX(angleRad, R3, x);
            tmp2 = skrotY(angleRad, R3, y);
            tmp3 = cosBeta(tmp1, tmp2, R1, R2);
            tmp4 = sinBeta(tmp3);
            checkBeta = a(tmp1, tmp2, R1 + R2 * tmp3, R2 * tmp4);
            checkGamma = b(tmp4, tmp3);
            checkDelta = c(checkBeta, checkGamma, angleRad);
            if (x * x + y * y > Math.Pow(R1 + R2 + R3, 2) || (tmp1 * tmp1 + tmp2 * tmp2 > Math.Pow(R1 + R2, 2)) || checkBeta > 2.792526803 || checkBeta < 0.34906585 || checkGamma > 0.34906585 || checkGamma < -2.792526803 || checkDelta > 2.792526803 || checkDelta < 0.34906585)
            {
                OnMsgShow?.Invoke("It's physically impossible");
                return false;
            }
            return true;
        }

        public void AddToAllLocationsList(string locationName)
        {
            
            {
                if (!allLocationsList.Contains(locationName))
                {
                    allLocationsList.Add(locationName);
                    coords.Add(new Location(locationName, Math.Round(alpha * 180 / Math.PI), Math.Round(beta * 180 / Math.PI), Math.Round(gamma * 180 / Math.PI), Math.Round(delta * 180 / Math.PI), Math.Round(rot * 180 / Math.PI), Math.Round(grab * 180 / Math.PI)));
                    OnAllLocationsListChange?.Invoke();
                    OnMsgShow(Math.Round(alpha * 180 / Math.PI).ToString() + Environment.NewLine + Math.Round(beta * 180 / Math.PI).ToString() + Environment.NewLine + Math.Round(gamma * 180 / Math.PI).ToString() + Environment.NewLine + Math.Round(delta * 180 / Math.PI).ToString() + Environment.NewLine + Math.Round(rot * 180 / Math.PI).ToString() + Environment.NewLine + Math.Round(grab * 180 / Math.PI).ToString() + Environment.NewLine);
                }
                else
                    OnMsgShow?.Invoke("location name must be unique");
            }

        }        
        public void AddToSelectedLocationsList(string locationName)
        {

            try
            {
                selectedLocationsList.Add(locationName);
                OnSelectedLocationsListChange?.Invoke();
            }
            catch
            {

            }

        }
        public void AddToSelectedLocationsList(int locationPosition)
        {

            try
            {
                selectedLocationsList.Add(allLocationsList[locationPosition]);
                OnSelectedLocationsListChange?.Invoke();
            }
            catch
            {

            }

        }
        public void RemoveFromSelectedLocationsList(string locationName)
        {
            try
            {
                selectedLocationsList.Remove(locationName);
                OnSelectedLocationsListChange?.Invoke();
            }
            catch
            {
                OnMsgShow?.Invoke("Selected location doesn't exist");
            }

        }
        public void RemoveFromSelectedLocationsList(int locationPosition)
        {
            try
            {
                selectedLocationsList.RemoveAt(locationPosition);
                OnSelectedLocationsListChange?.Invoke();
            }
            catch
            {
                OnMsgShow?.Invoke("Selected location doesn't exist");
            }


        }
        public void AddMacro(string macroName)
        {

       
                {
                    macroList.Add(macroName);
                    OnMacroListChange?.Invoke();
                    macro m = new macro(macroName);
                    foreach (var x in selectedLocationsList)
                        m.Add(coords.First(y => y.name == x));
                    makra.Add(m);
                }

            
        }
        public void RemoveLocationFromAllLocationsList(string LocationName)
        {

            try
            {
                if (coords != null) coords.Remove(coords.First(q => q.name == LocationName));
                allLocationsList.Remove(LocationName);
                OnAllLocationsListChange?.Invoke();
            }
            catch
            {
                OnMsgShow?.Invoke("Selected location doesn't exist");
            }

        }
        public void RemoveLocationFromAllLocationsList(int position)
        {

            try
            {
                if (coords != null && position < coords.Count)
                {
                    coords.RemoveAt(position);
                    allLocationsList.RemoveAt(position);
                }
                OnAllLocationsListChange?.Invoke();
            }
            catch
            {
                OnMsgShow?.Invoke("Selected location doesn't exist");
            }

        }
        public void DeleteMacro(string macroName)
        {

            try
            {
                if (makra != null) makra.Remove(makra.First(q => q.name == macroName));
                macroList.Remove(macroName);
                OnMacroListChange?.Invoke();
            }
            catch
            {
                OnMsgShow?.Invoke("Selected macro doesn't exist");
            }

        }
        public void DeleteMacro(int position)
        {

            try
            {
                if (makra != null && makra.Count > position)
                {
                    makra.RemoveAt(position);
                    macroList.RemoveAt(position);
                }
                OnMacroListChange?.Invoke();
            }
            catch
            {
                OnMsgShow?.Invoke("Selected macro doesn't exist");
            }

        }

        public void Refresh()
        {
            OnAllLocationsListChange?.Invoke();
            OnSelectedLocationsListChange?.Invoke();
            OnMacroListChange?.Invoke();
        }
    }
}

