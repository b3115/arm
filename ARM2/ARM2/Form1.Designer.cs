﻿namespace ARM2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.YUp = new System.Windows.Forms.Button();
            this.XLeft = new System.Windows.Forms.Button();
            this.YDown = new System.Windows.Forms.Button();
            this.XRight = new System.Windows.Forms.Button();
            this.BaseClockwise = new System.Windows.Forms.Button();
            this.BaseCclocwise = new System.Windows.Forms.Button();
            this.AngleUp = new System.Windows.Forms.Button();
            this.AngleDown = new System.Windows.Forms.Button();
            this.RotClockwise = new System.Windows.Forms.Button();
            this.RotCclockwise = new System.Windows.Forms.Button();
            this.Grab = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LocName = new System.Windows.Forms.TextBox();
            this.AddLoc = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.LocationList = new System.Windows.Forms.ListBox();
            this.MacroLocationsList = new System.Windows.Forms.ListBox();
            this.AddLocToMacro = new System.Windows.Forms.Button();
            this.DelLocFromMacro = new System.Windows.Forms.Button();
            this.MacroList = new System.Windows.Forms.ListBox();
            this.addMacro = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.Connections = new System.Windows.Forms.Button();
            this.COMList = new System.Windows.Forms.ListBox();
            this.Connect = new System.Windows.Forms.Button();
            this.MacroName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Abort = new System.Windows.Forms.Button();
            this.delLocButton = new System.Windows.Forms.Button();
            this.delMacroBut = new System.Windows.Forms.Button();
            this.executeMacroButton = new System.Windows.Forms.Button();
            this.firstLabel = new System.Windows.Forms.Label();
            this.first = new System.Windows.Forms.TextBox();
            this.secondLabel = new System.Windows.Forms.Label();
            this.thirdLabel = new System.Windows.Forms.Label();
            this.second = new System.Windows.Forms.TextBox();
            this.third = new System.Windows.Forms.TextBox();
            this.angspeed = new System.Windows.Forms.TextBox();
            this.speed = new System.Windows.Forms.TextBox();
            this.speedLabel = new System.Windows.Forms.Label();
            this.angspeedLabel = new System.Windows.Forms.Label();
            this.save = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.sendCurrent = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Grab)).BeginInit();
            this.SuspendLayout();
            // 
            // YUp
            // 
            this.YUp.Location = new System.Drawing.Point(93, 49);
            this.YUp.Name = "YUp";
            this.YUp.Size = new System.Drawing.Size(75, 23);
            this.YUp.TabIndex = 0;
            this.YUp.Text = "Y +";
            this.YUp.UseVisualStyleBackColor = true;
            this.YUp.Click += new System.EventHandler(this.YUp_Click);
            // 
            // XLeft
            // 
            this.XLeft.Location = new System.Drawing.Point(12, 78);
            this.XLeft.Name = "XLeft";
            this.XLeft.Size = new System.Drawing.Size(75, 23);
            this.XLeft.TabIndex = 1;
            this.XLeft.Text = "X -";
            this.XLeft.UseVisualStyleBackColor = true;
            this.XLeft.Click += new System.EventHandler(this.XLeft_Click);
            // 
            // YDown
            // 
            this.YDown.Location = new System.Drawing.Point(93, 107);
            this.YDown.Name = "YDown";
            this.YDown.Size = new System.Drawing.Size(75, 23);
            this.YDown.TabIndex = 2;
            this.YDown.Text = "Y -";
            this.YDown.UseVisualStyleBackColor = true;
            this.YDown.Click += new System.EventHandler(this.YDown_Click);
            // 
            // XRight
            // 
            this.XRight.Location = new System.Drawing.Point(174, 78);
            this.XRight.Name = "XRight";
            this.XRight.Size = new System.Drawing.Size(75, 23);
            this.XRight.TabIndex = 3;
            this.XRight.Text = "X +";
            this.XRight.UseVisualStyleBackColor = true;
            this.XRight.Click += new System.EventHandler(this.XRight_Click);
            // 
            // BaseClockwise
            // 
            this.BaseClockwise.Location = new System.Drawing.Point(275, 49);
            this.BaseClockwise.Name = "BaseClockwise";
            this.BaseClockwise.Size = new System.Drawing.Size(75, 23);
            this.BaseClockwise.TabIndex = 4;
            this.BaseClockwise.Text = "clockwise";
            this.BaseClockwise.UseVisualStyleBackColor = true;
            this.BaseClockwise.Click += new System.EventHandler(this.BaseClockwise_Click);
            // 
            // BaseCclocwise
            // 
            this.BaseCclocwise.Location = new System.Drawing.Point(275, 107);
            this.BaseCclocwise.Name = "BaseCclocwise";
            this.BaseCclocwise.Size = new System.Drawing.Size(75, 23);
            this.BaseCclocwise.TabIndex = 5;
            this.BaseCclocwise.Text = "C-clockwise";
            this.BaseCclocwise.UseVisualStyleBackColor = true;
            this.BaseCclocwise.Click += new System.EventHandler(this.BaseCclockwise_Click);
            // 
            // AngleUp
            // 
            this.AngleUp.Location = new System.Drawing.Point(409, 49);
            this.AngleUp.Name = "AngleUp";
            this.AngleUp.Size = new System.Drawing.Size(75, 23);
            this.AngleUp.TabIndex = 6;
            this.AngleUp.Text = "up";
            this.AngleUp.UseVisualStyleBackColor = true;
            this.AngleUp.Click += new System.EventHandler(this.AngleUp_Click);
            // 
            // AngleDown
            // 
            this.AngleDown.Location = new System.Drawing.Point(409, 107);
            this.AngleDown.Name = "AngleDown";
            this.AngleDown.Size = new System.Drawing.Size(75, 23);
            this.AngleDown.TabIndex = 7;
            this.AngleDown.Text = "down";
            this.AngleDown.UseVisualStyleBackColor = true;
            this.AngleDown.Click += new System.EventHandler(this.AngleDown_Click);
            // 
            // RotClockwise
            // 
            this.RotClockwise.Location = new System.Drawing.Point(542, 49);
            this.RotClockwise.Name = "RotClockwise";
            this.RotClockwise.Size = new System.Drawing.Size(75, 23);
            this.RotClockwise.TabIndex = 8;
            this.RotClockwise.Text = "clockwise";
            this.RotClockwise.UseVisualStyleBackColor = true;
            this.RotClockwise.Click += new System.EventHandler(this.RotClockwise_Click);
            // 
            // RotCclockwise
            // 
            this.RotCclockwise.Location = new System.Drawing.Point(542, 107);
            this.RotCclockwise.Name = "RotCclockwise";
            this.RotCclockwise.Size = new System.Drawing.Size(75, 23);
            this.RotCclockwise.TabIndex = 9;
            this.RotCclockwise.Text = "C-clockwise";
            this.RotCclockwise.UseVisualStyleBackColor = true;
            this.RotCclockwise.Click += new System.EventHandler(this.RotCclockwise_Click);
            // 
            // Grab
            // 
            this.Grab.Location = new System.Drawing.Point(623, 78);
            this.Grab.Maximum = 45;
            this.Grab.Name = "Grab";
            this.Grab.Size = new System.Drawing.Size(104, 45);
            this.Grab.TabIndex = 10;
            this.Grab.Value = 1;
            this.Grab.Scroll += new System.EventHandler(this.Grab_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "X Y";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(267, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "angle on X-Y plane";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(375, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "angle of the last joint to ground";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(539, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Rotate grabber";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(641, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Grab/Ungrab";
            // 
            // LocName
            // 
            this.LocName.Location = new System.Drawing.Point(6, 242);
            this.LocName.Name = "LocName";
            this.LocName.Size = new System.Drawing.Size(91, 20);
            this.LocName.TabIndex = 30;
            // 
            // AddLoc
            // 
            this.AddLoc.Location = new System.Drawing.Point(6, 265);
            this.AddLoc.Name = "AddLoc";
            this.AddLoc.Size = new System.Drawing.Size(91, 23);
            this.AddLoc.TabIndex = 31;
            this.AddLoc.Text = "Add to listbox";
            this.AddLoc.UseVisualStyleBackColor = true;
            this.AddLoc.Click += new System.EventHandler(this.AddLoc_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 226);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "New location";
            // 
            // LocationList
            // 
            this.LocationList.FormattingEnabled = true;
            this.LocationList.Location = new System.Drawing.Point(103, 226);
            this.LocationList.Name = "LocationList";
            this.LocationList.Size = new System.Drawing.Size(120, 95);
            this.LocationList.TabIndex = 33;
            this.LocationList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDoubleClick);
            // 
            // MacroLocationsList
            // 
            this.MacroLocationsList.FormattingEnabled = true;
            this.MacroLocationsList.Location = new System.Drawing.Point(282, 226);
            this.MacroLocationsList.Name = "MacroLocationsList";
            this.MacroLocationsList.Size = new System.Drawing.Size(120, 95);
            this.MacroLocationsList.TabIndex = 34;
            // 
            // AddLocToMacro
            // 
            this.AddLocToMacro.Location = new System.Drawing.Point(229, 226);
            this.AddLocToMacro.Name = "AddLocToMacro";
            this.AddLocToMacro.Size = new System.Drawing.Size(47, 23);
            this.AddLocToMacro.TabIndex = 35;
            this.AddLocToMacro.Text = "---->";
            this.AddLocToMacro.UseVisualStyleBackColor = true;
            this.AddLocToMacro.Click += new System.EventHandler(this.AddLocToMacro_Click);
            // 
            // DelLocFromMacro
            // 
            this.DelLocFromMacro.Location = new System.Drawing.Point(229, 298);
            this.DelLocFromMacro.Name = "DelLocFromMacro";
            this.DelLocFromMacro.Size = new System.Drawing.Size(47, 23);
            this.DelLocFromMacro.TabIndex = 36;
            this.DelLocFromMacro.Text = "<----";
            this.DelLocFromMacro.UseVisualStyleBackColor = true;
            this.DelLocFromMacro.Click += new System.EventHandler(this.DelLocFromMacro_Click);
            // 
            // MacroList
            // 
            this.MacroList.FormattingEnabled = true;
            this.MacroList.Location = new System.Drawing.Point(409, 226);
            this.MacroList.Name = "MacroList";
            this.MacroList.Size = new System.Drawing.Size(120, 95);
            this.MacroList.TabIndex = 37;
            this.MacroList.SelectedIndexChanged += new System.EventHandler(this.listBox3_SelectedIndexChanged);
            // 
            // addMacro
            // 
            this.addMacro.Location = new System.Drawing.Point(535, 265);
            this.addMacro.Name = "addMacro";
            this.addMacro.Size = new System.Drawing.Size(75, 23);
            this.addMacro.TabIndex = 38;
            this.addMacro.Text = "Add macro";
            this.addMacro.UseVisualStyleBackColor = true;
            this.addMacro.Click += new System.EventHandler(this.button14_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.RtsEnable = true;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // Connections
            // 
            this.Connections.Location = new System.Drawing.Point(12, 158);
            this.Connections.Name = "Connections";
            this.Connections.Size = new System.Drawing.Size(75, 23);
            this.Connections.TabIndex = 45;
            this.Connections.Text = "Connections";
            this.Connections.UseVisualStyleBackColor = true;
            this.Connections.Click += new System.EventHandler(this.Connection);
            // 
            // COMList
            // 
            this.COMList.FormattingEnabled = true;
            this.COMList.Location = new System.Drawing.Point(93, 147);
            this.COMList.Name = "COMList";
            this.COMList.Size = new System.Drawing.Size(48, 43);
            this.COMList.TabIndex = 46;
            this.COMList.Visible = false;
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(147, 158);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(75, 23);
            this.Connect.TabIndex = 47;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Visible = false;
            this.Connect.Click += new System.EventHandler(this.ConnectButton);
            // 
            // MacroName
            // 
            this.MacroName.Location = new System.Drawing.Point(535, 239);
            this.MacroName.Name = "MacroName";
            this.MacroName.Size = new System.Drawing.Size(100, 20);
            this.MacroName.TabIndex = 48;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(689, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(23, 23);
            this.panel1.TabIndex = 49;
            // 
            // Abort
            // 
            this.Abort.Location = new System.Drawing.Point(608, 7);
            this.Abort.Name = "Abort";
            this.Abort.Size = new System.Drawing.Size(75, 23);
            this.Abort.TabIndex = 50;
            this.Abort.Text = "Abort";
            this.Abort.UseVisualStyleBackColor = true;
            this.Abort.Click += new System.EventHandler(this.Abort_Click);
            // 
            // delLocButton
            // 
            this.delLocButton.Location = new System.Drawing.Point(6, 294);
            this.delLocButton.Name = "delLocButton";
            this.delLocButton.Size = new System.Drawing.Size(91, 23);
            this.delLocButton.TabIndex = 49;
            this.delLocButton.Text = "Delete location";
            this.delLocButton.UseVisualStyleBackColor = true;
            this.delLocButton.Click += new System.EventHandler(this.delLocButton_Click);
            // 
            // delMacroBut
            // 
            this.delMacroBut.Location = new System.Drawing.Point(535, 294);
            this.delMacroBut.Name = "delMacroBut";
            this.delMacroBut.Size = new System.Drawing.Size(82, 23);
            this.delMacroBut.TabIndex = 50;
            this.delMacroBut.Text = "Delete macro";
            this.delMacroBut.UseVisualStyleBackColor = true;
            this.delMacroBut.Click += new System.EventHandler(this.delMacroBut_Click);
            // 
            // executeMacroButton
            // 
            this.executeMacroButton.Location = new System.Drawing.Point(616, 265);
            this.executeMacroButton.Name = "executeMacroButton";
            this.executeMacroButton.Size = new System.Drawing.Size(63, 23);
            this.executeMacroButton.TabIndex = 57;
            this.executeMacroButton.Text = "execute";
            this.executeMacroButton.UseVisualStyleBackColor = true;
            this.executeMacroButton.Click += new System.EventHandler(this.executeMacroButton_Click);
            // 
            // firstLabel
            // 
            this.firstLabel.AutoSize = true;
            this.firstLabel.Location = new System.Drawing.Point(12, 12);
            this.firstLabel.Name = "firstLabel";
            this.firstLabel.Size = new System.Drawing.Size(23, 13);
            this.firstLabel.TabIndex = 58;
            this.firstLabel.Text = "first";
            // 
            // first
            // 
            this.first.Location = new System.Drawing.Point(37, 9);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(44, 20);
            this.first.TabIndex = 59;
            this.first.Text = "180";
            // 
            // secondLabel
            // 
            this.secondLabel.AutoSize = true;
            this.secondLabel.Location = new System.Drawing.Point(87, 12);
            this.secondLabel.Name = "secondLabel";
            this.secondLabel.Size = new System.Drawing.Size(42, 13);
            this.secondLabel.TabIndex = 60;
            this.secondLabel.Text = "second";
            // 
            // thirdLabel
            // 
            this.thirdLabel.AutoSize = true;
            this.thirdLabel.Location = new System.Drawing.Point(185, 12);
            this.thirdLabel.Name = "thirdLabel";
            this.thirdLabel.Size = new System.Drawing.Size(27, 13);
            this.thirdLabel.TabIndex = 61;
            this.thirdLabel.Text = "third";
            // 
            // second
            // 
            this.second.Location = new System.Drawing.Point(135, 9);
            this.second.Name = "second";
            this.second.Size = new System.Drawing.Size(43, 20);
            this.second.TabIndex = 62;
            this.second.Text = "120";
            // 
            // third
            // 
            this.third.Location = new System.Drawing.Point(218, 9);
            this.third.Name = "third";
            this.third.Size = new System.Drawing.Size(48, 20);
            this.third.TabIndex = 63;
            this.third.Text = "130";
            // 
            // angspeed
            // 
            this.angspeed.Location = new System.Drawing.Point(449, 9);
            this.angspeed.Name = "angspeed";
            this.angspeed.Size = new System.Drawing.Size(46, 20);
            this.angspeed.TabIndex = 64;
            this.angspeed.Text = "2";
            // 
            // speed
            // 
            this.speed.Location = new System.Drawing.Point(314, 9);
            this.speed.Name = "speed";
            this.speed.Size = new System.Drawing.Size(49, 20);
            this.speed.TabIndex = 65;
            this.speed.Text = "10";
            // 
            // speedLabel
            // 
            this.speedLabel.AutoSize = true;
            this.speedLabel.Location = new System.Drawing.Point(272, 12);
            this.speedLabel.Name = "speedLabel";
            this.speedLabel.Size = new System.Drawing.Size(36, 13);
            this.speedLabel.TabIndex = 66;
            this.speedLabel.Text = "speed";
            // 
            // angspeedLabel
            // 
            this.angspeedLabel.AutoSize = true;
            this.angspeedLabel.Location = new System.Drawing.Point(369, 12);
            this.angspeedLabel.Name = "angspeedLabel";
            this.angspeedLabel.Size = new System.Drawing.Size(74, 13);
            this.angspeedLabel.TabIndex = 67;
            this.angspeedLabel.Text = "angular speed";
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(501, 7);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 68;
            this.save.Text = "save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(100, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 69;
            this.label6.Text = "locations";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(295, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 70;
            this.label7.Text = "locations in macro";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(446, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 71;
            this.label8.Text = "macros";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(551, 222);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 72;
            this.label9.Text = "new macro";
            // 
            // sendCurrent
            // 
            this.sendCurrent.Location = new System.Drawing.Point(542, 143);
            this.sendCurrent.Name = "sendCurrent";
            this.sendCurrent.Size = new System.Drawing.Size(75, 52);
            this.sendCurrent.TabIndex = 73;
            this.sendCurrent.Text = "send";
            this.sendCurrent.UseVisualStyleBackColor = true;
            this.sendCurrent.Click += new System.EventHandler(this.sendCurrent_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 333);
            this.Controls.Add(this.sendCurrent);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.save);
            this.Controls.Add(this.angspeedLabel);
            this.Controls.Add(this.speedLabel);
            this.Controls.Add(this.speed);
            this.Controls.Add(this.angspeed);
            this.Controls.Add(this.third);
            this.Controls.Add(this.second);
            this.Controls.Add(this.thirdLabel);
            this.Controls.Add(this.secondLabel);
            this.Controls.Add(this.first);
            this.Controls.Add(this.firstLabel);
            this.Controls.Add(this.executeMacroButton);
            this.Controls.Add(this.Abort);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.delMacroBut);
            this.Controls.Add(this.delLocButton);
            this.Controls.Add(this.MacroName);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.COMList);
            this.Controls.Add(this.Connections);
            this.Controls.Add(this.addMacro);
            this.Controls.Add(this.MacroList);
            this.Controls.Add(this.DelLocFromMacro);
            this.Controls.Add(this.AddLocToMacro);
            this.Controls.Add(this.MacroLocationsList);
            this.Controls.Add(this.LocationList);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.AddLoc);
            this.Controls.Add(this.LocName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Grab);
            this.Controls.Add(this.RotCclockwise);
            this.Controls.Add(this.RotClockwise);
            this.Controls.Add(this.AngleDown);
            this.Controls.Add(this.AngleUp);
            this.Controls.Add(this.BaseCclocwise);
            this.Controls.Add(this.BaseClockwise);
            this.Controls.Add(this.XRight);
            this.Controls.Add(this.YDown);
            this.Controls.Add(this.XLeft);
            this.Controls.Add(this.YUp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.Form1_Layout);
            ((System.ComponentModel.ISupportInitialize)(this.Grab)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button YUp;
        private System.Windows.Forms.Button XLeft;
        private System.Windows.Forms.Button YDown;
        private System.Windows.Forms.Button XRight;
        private System.Windows.Forms.Button BaseClockwise;
        private System.Windows.Forms.Button BaseCclocwise;
        private System.Windows.Forms.Button AngleUp;
        private System.Windows.Forms.Button AngleDown;
        private System.Windows.Forms.Button RotClockwise;
        private System.Windows.Forms.Button RotCclockwise;
        private System.Windows.Forms.TrackBar Grab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox LocName;
        private System.Windows.Forms.Button AddLoc;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ListBox LocationList;
        private System.Windows.Forms.ListBox MacroLocationsList;
        private System.Windows.Forms.Button AddLocToMacro;
        private System.Windows.Forms.Button DelLocFromMacro;
        private System.Windows.Forms.ListBox MacroList;
        private System.Windows.Forms.Button addMacro;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button Connections;
        private System.Windows.Forms.ListBox COMList;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.TextBox MacroName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Abort;
        private System.Windows.Forms.Button delLocButton;
        private System.Windows.Forms.Button delMacroBut;
        private System.Windows.Forms.Button executeMacroButton;
        private System.Windows.Forms.Label firstLabel;
        private System.Windows.Forms.TextBox first;
        private System.Windows.Forms.Label secondLabel;
        private System.Windows.Forms.Label thirdLabel;
        private System.Windows.Forms.TextBox second;
        private System.Windows.Forms.TextBox third;
        private System.Windows.Forms.TextBox angspeed;
        private System.Windows.Forms.TextBox speed;
        private System.Windows.Forms.Label speedLabel;
        private System.Windows.Forms.Label angspeedLabel;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button sendCurrent;
    }
}

