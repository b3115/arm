using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using Android.Bluetooth;
using System.IO;
using System.Threading;

namespace App1.Droid
{
     delegate void  OnBluetoothReadEvent(char c);
    class BluetoothConnection
    {
        BluetoothAdapter mmAdapter = BluetoothAdapter.DefaultAdapter;
        BluetoothSocket mmSocket = null;
        BluetoothServerSocket mmServerSocket = null;
        BluetoothDevice mmDevice;
        public OnBluetoothReadEvent OnBluetoothRead;
        UUID my_UUID = UUID.FromString("00001101-0000-1000-8000-00805f9b34fb");
        bool isListening = false;
        public bool isConnected = false;
        byte[] readBuffer = new byte[1024];
        int bufferPointer = 0;
        int readCount = 0;

       

        public void ConnectWith(string deviceName)
        {
            var pairedDevices = mmAdapter.BondedDevices;
            if (pairedDevices.Count > 0)
            {

                BluetoothDevice device = pairedDevices.First(x => x.Name == deviceName);
                ShowMessage("Znalaz�em " + device.Name);
                ConnectAsClient(device);
                if (mmSocket != null)
                    isConnected = true;
                else
                    isConnected = false;

            }
            else
            {
                ShowMessage("err");
            }
        }
        private void ShowMessage(String msg)
        {
            Java.Lang.ICharSequence text = new Java.Lang.String(msg);
            ToastLength duration = ToastLength.Short;
            Toast toast = Toast.MakeText(Application.Context, text, duration);
            toast.Show();
        }
        private void ConnectAsClient(BluetoothDevice device)
        {
            BluetoothSocket tmp = null;
            mmDevice = device;
            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try
            {
                tmp = device.CreateRfcommSocketToServiceRecord(my_UUID);
            }
            catch (Java.IO.IOException e) { }
            mmSocket = tmp;

            try
            {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.Connect();
            }
            catch (Java.IO.IOException connectException)
            {
                // Unable to connect; close the socket and get out
                try
                {
                    mmSocket.Close();
                    ShowMessage("Socket Closed");
                }
                catch (Java.IO.IOException closeException)
                {
                    ShowMessage("Socket Closing err");
                }
                return;
            }

           
           
        }
        public void Send(String msg)
        {
            try
            {
                byte[] bytes = new byte[1000];
                Stream mmOut = mmSocket.OutputStream;
                for(int i=0;i<msg.Count();i++)
                {
                    bytes[i] = Convert.ToByte(msg[i]);
                }
                
                mmOut.Write(bytes, 0, msg.Count());
            }
            catch (Java.IO.IOException e)
            {
                ShowMessage("sending error");
            }
        }
        public string Read(int count)
        {
            StringBuilder sb = new StringBuilder();
            count = count > readCount ? readCount : count;
            for(;count>0;count--)
            {
                readCount--;
                sb.Append(Convert.ToChar(readBuffer[bufferPointer]));
                bufferPointer = (bufferPointer + 1) % 1024;
            }
            return sb.ToString();
        }
        public void ReadFromDevice()
        {
            byte b;
            Stream mmIn = mmSocket.InputStream;
            b=readBuffer[(bufferPointer + readCount) % 1024] = Convert.ToByte(mmIn.ReadByte());
            OnBluetoothRead?.Invoke(Convert.ToChar(b));
            readCount++;
         //   ShowMessage(Convert.ToString(Convert.ToChar(readBuffer[(bufferPointer + readCount) % 1024])));
        }

        public void Run()
        {
            while (true)
            {
                ReadFromDevice();
            }
        }
   
        public void StartListenning()
        {
            isListening = true;
            ThreadStart myThreadDelegate = new ThreadStart(this.Run);
            Thread myThread = new Thread(myThreadDelegate);
            myThread.Start();
        }
    }
}