﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Content.PM;
using ARM2;

namespace App1.Droid
{
    [Activity (Label = "App1.Droid", MainLauncher = true, Icon = "@drawable/icon",ScreenOrientation = ScreenOrientation.Landscape)]
 
    public class MainActivity : Activity
	{
        int allLocationListPosition;
        int selectedLocationListPosition;
        int macroListPosition;
        bool blockAdding = false;
        bool isBusy = false;
        Queue<Location> q = new Queue<Location>();
        List<String> xD = new List<String>();
        ViewModel vm = new ViewModel();
        Button xPlusButton; //= FindViewById<Button>(Resource.Id.XPlusButton);
        Button yPlusButton; //= FindViewById<Button>(Resource.Id.YPlusButton);
        Button xMinusButton; //= FindViewById<Button>(Resource.Id.XMinusButton);
        Button yMinusButton; //= FindViewById<Button>(Resource.Id.YMinusButton);
        Button xyClockwiseButton; //= FindViewById<Button>(Resource.Id.XYClockwiseButton);
        Button xyCClockwiseButton;// = FindViewById<Button>(Resource.Id.XYCClockwiseButton);
        Button lastJointUpButton; //= FindViewById<Button>(Resource.Id.LastJointUpButton);
        Button lastJointDownButton; //= FindViewById<Button>(Resource.Id.LastJointDownButton);
        Button grabClockwiseButton; //= FindViewById<Button>(Resource.Id.GrabClockwiseButton);
        Button grabCClockwiseButton; //= FindViewById<Button>(Resource.Id.GrabCClockwiseButton);
        Button connectButton;
        Button executeMacroButton;
        Button allLocationListAddButton; //= a.FindViewById<Button>(Resource.Id.AllLocationListAddButton);
        Button allLocationListRemoveButton;
        Button selectedLocationListAddButton;// = a.FindViewById<Button>(Resource.Id.SelectedLocationListAddButton);
        Button selectedLocationListRemoveButton;// = a.FindViewById<Button>(Resource.Id.SelectedLocationListRemoveButton);
        Button macroListAddButton;// = a.FindViewById<Button>(Resource.Id.MacroListAddButton);
        Button macroListRemoveButton;// = a.FindViewById<Button>(Resource.Id.MacroListRemoveButton);
        Spinner allLocationListSpinner; //= FindViewById<Spinner>(Resource.Id.AllLocationSpinner);
        Spinner selectedLocationListSpinner; //= FindViewById<Spinner>(Resource.Id.SelectedLocationSpinner);
        Spinner toMacroListSpinner; //= FindViewById<Spinner>(Resource.Id.ToMacroListSpinner);
        TextView baseTextLabel; //= FindViewById<TextView>(Resource.Id.BaseTextLabel);
        TextView base0Label; //= FindViewById<TextView>(Resource.Id.Base0Label);
        TextView base1Label; //= FindViewById<TextView>(Resource.Id.Base1Label);
        TextView firstJointTextLabel; //= FindViewById<TextView>(Resource.Id.FirstJointTextLabel);
        TextView firstJoint0Label; //= FindViewById<TextView>(Resource.Id.FirstJoint0Label);
        TextView firstJoint1Label; //= FindViewById<TextView>(Resource.Id.FirstJoint1Label);
        TextView secondJointTextLabel; //= FindViewById<TextView>(Resource.Id.SecondJointTextLabel);
        TextView secondJoint0Label; //= FindViewById<TextView>(Resource.Id.SecondJoint0Label);
        TextView secondJoint1Label; //= FindViewById<TextView>(Resource.Id.SecondJoint1Label);
        TextView thirdJointTextLabel; //= FindViewById<TextView>(Resource.Id.ThirdJointTextLabel);
        TextView thirdJoint0Label; //= FindViewById<TextView>(Resource.Id.ThirdJoint0Label);
        TextView thirdJoint1Label; //= FindViewById<TextView>(Resource.Id.ThirdJoint1Label);
        TextView rotateTextLabel; //= FindViewById<TextView>(Resource.Id.RotateTextLabel);
        TextView rotate0Label; //= FindViewById<TextView>(Resource.Id.Rotate0Label);
        TextView rotate1Label; //= FindViewById<TextView>(Resource.Id.Rotate1Label);
        TextView grabTextLabel; //= FindViewById<TextView>(Resource.Id.GrabTextLabel);
        TextView grab0Label; //= FindViewById<TextView>(Resource.Id.Grab0Label);
        TextView grab1Label; //= FindViewById<TextView>(Resource.Id.Grab1Label);
        TextView x0Label; //= FindViewById<TextView>(Resource.Id.X0Label);
        TextView x1Label; //= FindViewById<TextView>(Resource.Id.X1Label);
        TextView y0Label; //= FindViewById<TextView>(Resource.Id.Y0Label);
        TextView y1Label; //= FindViewById<TextView>(Resource.Id.Y1Label);
        TextView z0Label; //= FindViewById<TextView>(Resource.Id.Z0Label);
        TextView z1Label; //= FindViewById<TextView>(Resource.Id.Z1Label);
        TextView completeStringTextLabel; //= FindViewById<TextView>(Resource.Id.CompleteStringTextLabel);
        TextView completeStringValueLabel; //= FindViewById<TextView>(Resource.Id.CompleteStringValueLabel);
        CheckBox deleteCheckBox;
        EditText locationNameEditText; //= a.FindViewById<EditText>(Resource.Id.LocationNameEditText);
        BluetoothConnection bc = new BluetoothConnection();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            xPlusButton = FindViewById<Button>(Resource.Id.XPlusButton);
            yPlusButton = FindViewById<Button>(Resource.Id.YPlusButton);
            xMinusButton = FindViewById<Button>(Resource.Id.XMinusButton);
            yMinusButton = FindViewById<Button>(Resource.Id.YMinusButton);
            xyClockwiseButton = FindViewById<Button>(Resource.Id.XYClockwiseButton);
            xyCClockwiseButton = FindViewById<Button>(Resource.Id.XYCClockwiseButton);
            lastJointUpButton = FindViewById<Button>(Resource.Id.LastJointUpButton);
            lastJointDownButton = FindViewById<Button>(Resource.Id.LastJointDownButton);
            grabClockwiseButton = FindViewById<Button>(Resource.Id.GrabClockwiseButton);
            grabCClockwiseButton = FindViewById<Button>(Resource.Id.GrabCClockwiseButton);
            connectButton = FindViewById<Button>(Resource.Id.ConnectButton);
            executeMacroButton = FindViewById<Button>(Resource.Id.ExecuteMacroButton);
            allLocationListAddButton = FindViewById<Button>(Resource.Id.AllLocationListAddButton);
            allLocationListRemoveButton = FindViewById<Button>(Resource.Id.AllLocationListRemoveButton);
            selectedLocationListAddButton = FindViewById<Button>(Resource.Id.SelectedLocationListAddButton);
            selectedLocationListRemoveButton = FindViewById<Button>(Resource.Id.SelectedLocationListRemoveButton);
            macroListAddButton = FindViewById<Button>(Resource.Id.MacroListAddButton);
            macroListRemoveButton = FindViewById<Button>(Resource.Id.MacroListRemoveButton);
            allLocationListSpinner = FindViewById<Spinner>(Resource.Id.AllLocationSpinner);
            selectedLocationListSpinner = FindViewById<Spinner>(Resource.Id.SelectedLocationSpinner);
            toMacroListSpinner = FindViewById<Spinner>(Resource.Id.ToMacroListSpinner);
            baseTextLabel = FindViewById<TextView>(Resource.Id.BaseTextLabel);
            base0Label = FindViewById<TextView>(Resource.Id.Base0Label);
            base1Label = FindViewById<TextView>(Resource.Id.Base1Label);
            firstJointTextLabel = FindViewById<TextView>(Resource.Id.FirstJointTextLabel);
            firstJoint0Label = FindViewById<TextView>(Resource.Id.FirstJoint0Label);
            firstJoint1Label = FindViewById<TextView>(Resource.Id.FirstJoint1Label);
            secondJointTextLabel = FindViewById<TextView>(Resource.Id.SecondJointTextLabel);
            secondJoint0Label = FindViewById<TextView>(Resource.Id.SecondJoint0Label);
            secondJoint1Label = FindViewById<TextView>(Resource.Id.SecondJoint1Label);
            thirdJointTextLabel = FindViewById<TextView>(Resource.Id.ThirdJointTextLabel);
            thirdJoint0Label = FindViewById<TextView>(Resource.Id.ThirdJoint0Label);
            thirdJoint1Label = FindViewById<TextView>(Resource.Id.ThirdJoint1Label);
            rotateTextLabel = FindViewById<TextView>(Resource.Id.RotateTextLabel);
            rotate0Label = FindViewById<TextView>(Resource.Id.Rotate0Label);
            rotate1Label = FindViewById<TextView>(Resource.Id.Rotate1Label);
            grabTextLabel = FindViewById<TextView>(Resource.Id.GrabTextLabel);
            grab0Label = FindViewById<TextView>(Resource.Id.Grab0Label);
            grab1Label = FindViewById<TextView>(Resource.Id.Grab1Label);
            x0Label = FindViewById<TextView>(Resource.Id.X0Label);
            x1Label = FindViewById<TextView>(Resource.Id.X1Label);
            y0Label = FindViewById<TextView>(Resource.Id.Y0Label);
            y1Label = FindViewById<TextView>(Resource.Id.Y1Label);
            z0Label = FindViewById<TextView>(Resource.Id.Z0Label);
            z1Label = FindViewById<TextView>(Resource.Id.Z1Label);
            completeStringTextLabel = FindViewById<TextView>(Resource.Id.CompleteStringTextLabel);
            completeStringValueLabel = FindViewById<TextView>(Resource.Id.CompleteStringValueLabel);
            deleteCheckBox = FindViewById<CheckBox>(Resource.Id.DeleteCheckBox);
            locationNameEditText = FindViewById<EditText>(Resource.Id.LocationNameEditText);



            Component.Setup(this);
            xPlusButton.Click += XPlusButton_Click;
            yPlusButton.Click += YPlusButton_Click;
            xMinusButton.Click += XMinusButton_Click;
            yMinusButton.Click += YMinusButton_Click;
            lastJointDownButton.Click += LastJointDownButton_Click;
            lastJointUpButton.Click += LastJointUpButton_Click;
            grabCClockwiseButton.Click += GrabCClockwiseButton_Click;
            grabClockwiseButton.Click += GrabClockwiseButton_Click;
            xyCClockwiseButton.Click += XYCClockwiseButton_Click;
            xyClockwiseButton.Click += XYClockwiseButton_Click;
            connectButton.Click += ConnectButton_Click;
            executeMacroButton.Click += ExecuteMacroButton_Click;
            allLocationListAddButton.Click += AllLocationListAddButton_Click;
            allLocationListRemoveButton.Click += AllLocationListRemoveButton_Click;
            selectedLocationListAddButton.Click += SelectedLocationListAddButton_Click;
            macroListAddButton.Click += MacroListAddButton_Click1;
            macroListRemoveButton.Click += MacroListRemoveButton_Click;

            allLocationListSpinner.ItemSelected += AllLocationListSpinner_ItemSelected;
            selectedLocationListSpinner.ItemSelected += SelectedLocationListSpinner_ItemSelected;
            toMacroListSpinner.ItemSelected += ToMacroListSpinner_ItemSelected;



            vm.OnAllLocationsListChange += () => { UpdateSpinner(allLocationListSpinner, vm.allLocationsList); };
            vm.OnMacroListChange += () => { UpdateSpinner(toMacroListSpinner, vm.macroList); };
            vm.OnSelectedLocationsListChange += () => { UpdateSpinner(selectedLocationListSpinner, vm.selectedLocationsList); };
            vm.OnChangeMade += Change;
            vm.OnMsgShow += ShowMessage;
            bc.OnBluetoothRead += (char c) =>
            {
                if (c == '3')
                {
                    if (q.Count > 0)
                    {
                        bc.Send(q.Dequeue().ToString());
                    }
                    else
                        isBusy = false;
                }
            };

        }

        private void MacroListRemoveButton_Click(object sender, EventArgs e)
        {
           vm.DeleteMacro(macroListPosition);
        }

        private void AllLocationListRemoveButton_Click(object sender, EventArgs e)
        {
            vm.RemoveLocationFromAllLocationsList(allLocationListPosition);
        }

        private void MacroListAddButton_Click1(object sender, EventArgs e)
        {
            vm.AddMacro(locationNameEditText.Text);
        }

        private void ToMacroListSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            macroListPosition = e.Position;
        }

        private void SelectedLocationListSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            selectedLocationListPosition = e.Position;
        }

        private void SelectedLocationListAddButton_Click(object sender, EventArgs e)
        {
            vm.AddToSelectedLocationsList(allLocationListPosition);
        }


        private void AllLocationListSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            allLocationListPosition = e.Position;
            if(deleteCheckBox.Checked)
            {
                if (!blockAdding)
                {
                    blockAdding = true;
                    vm.RemoveLocationFromAllLocationsList(e.Parent.GetItemAtPosition(e.Position).ToString());
                }
                else
                {
                    blockAdding = false;
                }
            }           
        }

       

        private void AllLocationListAddButton_Click(object sender, EventArgs e)
        {
            blockAdding = true;
            vm.AddToAllLocationsList(locationNameEditText.Text);
        }

        private void ExecuteMacroButton_Click(object sender, EventArgs e)
        {
            macro m = vm.makra[macroListPosition];
            foreach(var l in m)
            {
                q.Enqueue(l);
            }
            if (bc.isConnected)
            {
                if(!isBusy && q.Count>0)
                {
                    isBusy = true;
                    bc.Send(q.Dequeue().ToString());
                }
            }
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            bc.ConnectWith("HC-05");
            if (bc.isConnected)
                bc.StartListenning();
            else
                ShowMessage("Connection error");
        }

        private void Change()
        {
            x0Label.Text = vm.x.ToString();
            y0Label.Text = vm.y.ToString();
            z0Label.Text = vm.alpha.ToString();
            y1Label.Text = vm.angle.ToString();
            x1Label.Text = vm.rot.ToString();
            z1Label.Text = vm.grab.ToString();

            base0Label.Text = vm.tmp.alpha.ToString();
            firstJoint0Label.Text = vm.tmp.beta.ToString();
            secondJoint0Label.Text = vm.tmp.gamma.ToString();
            thirdJoint0Label.Text = vm.tmp.delta.ToString();
            rotate0Label.Text = vm.tmp.rot.ToString();
            grab0Label.Text = vm.tmp.grab.ToString();
            if (bc.isConnected && !isBusy)
            {
                bc.Send(vm.tmp.ToString());
                isBusy = true;
            }
        }
        private void XYClockwiseButton_Click(object sender, EventArgs e)
        {
            vm.IncreaseAlpha();
        }
        private void XYCClockwiseButton_Click(object sender, EventArgs e)
        {
            vm.DecreaseAlpha();
        }
        private void GrabClockwiseButton_Click(object sender, EventArgs e)
        {
            vm.IncreaseRot();
        }
        private void GrabCClockwiseButton_Click(object sender, EventArgs e)
        {
            vm.DecreaseRot();
        }
        private void LastJointUpButton_Click(object sender, EventArgs e)
        {
            vm.IncreaseAngle();
        }
        private void LastJointDownButton_Click(object sender, EventArgs e)
        {
            vm.DecreaseAngle();
        }
        private void YMinusButton_Click(object sender, EventArgs e)
        {
            vm.DecreaseY();
        }
        private void XMinusButton_Click(object sender, EventArgs e)
        {
            vm.DecreaseX();
        }
        private void YPlusButton_Click(object sender, EventArgs e)
        {
            vm.IncreaseY();
        }
        private void XPlusButton_Click(object sender, EventArgs e)
        {

            vm.IncreaseX();
        }

        private void UpdateSpinner(Spinner spinner, List<String> collection)
        {
           
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerItem, collection);
            spinner.Adapter = dataAdapter;
           
        }

        private void ShowMessage(String msg)
        {
            Java.Lang.ICharSequence text = new Java.Lang.String(msg);
            ToastLength duration = ToastLength.Short;
            Toast toast = Toast.MakeText(Application.Context, text, duration);
            toast.Show();
        }

    }
}


