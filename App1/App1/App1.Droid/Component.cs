using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace App1.Droid
{
    class Component
    {
        public static void Setup(Activity a)
        {
            Button xPlusButton = a.FindViewById<Button>(Resource.Id.XPlusButton);
            Button yPlusButton = a.FindViewById<Button>(Resource.Id.YPlusButton);
            Button xMinusButton = a.FindViewById<Button>(Resource.Id.XMinusButton);
            Button yMinusButton = a.FindViewById<Button>(Resource.Id.YMinusButton);
            Button xyClockwiseButton = a.FindViewById<Button>(Resource.Id.XYClockwiseButton);
            Button xyCClockwiseButton = a.FindViewById<Button>(Resource.Id.XYCClockwiseButton);
            Button lastJointUpButton = a.FindViewById<Button>(Resource.Id.LastJointUpButton);
            Button lastJointDownButton = a.FindViewById<Button>(Resource.Id.LastJointDownButton);
            Button grabClockwiseButton = a.FindViewById<Button>(Resource.Id.GrabClockwiseButton);
            Button grabCClockwiseButton = a.FindViewById<Button>(Resource.Id.GrabCClockwiseButton);
            Button connectButton = a.FindViewById<Button>(Resource.Id.ConnectButton);
            Button executeMacroButton = a.FindViewById<Button>(Resource.Id.ExecuteMacroButton);
            Button allLocationListAddButton = a.FindViewById<Button>(Resource.Id.AllLocationListAddButton);
            Button allLocationListRemoveButton = a.FindViewById<Button>(Resource.Id.AllLocationListRemoveButton);
            Button selectedLocationListAddButton = a.FindViewById<Button>(Resource.Id.SelectedLocationListAddButton);
            Button selectedLocationListRemoveButton = a.FindViewById<Button>(Resource.Id.SelectedLocationListRemoveButton);
            Button macroListAddButton = a.FindViewById<Button>(Resource.Id.MacroListAddButton);
            Button macroListRemoveButton = a.FindViewById<Button>(Resource.Id.MacroListRemoveButton);
            Spinner allLocationListSpinner = a.FindViewById<Spinner>(Resource.Id.AllLocationSpinner);
            Spinner selectedLocationListSpinner = a.FindViewById<Spinner>(Resource.Id.SelectedLocationSpinner);
            Spinner toMacroListSpinner = a.FindViewById<Spinner>(Resource.Id.ToMacroListSpinner);
            TextView baseTextLabel = a.FindViewById<TextView>(Resource.Id.BaseTextLabel);
            TextView base0Label = a.FindViewById<TextView>(Resource.Id.Base0Label);
            TextView base1Label = a.FindViewById<TextView>(Resource.Id.Base1Label);
            TextView firstJointTextLabel = a.FindViewById<TextView>(Resource.Id.FirstJointTextLabel);
            TextView firstJoint0Label = a.FindViewById<TextView>(Resource.Id.FirstJoint0Label);
            TextView firstJoint1Label = a.FindViewById<TextView>(Resource.Id.FirstJoint1Label);
            TextView secondJointTextLabel = a.FindViewById<TextView>(Resource.Id.SecondJointTextLabel);
            TextView secondJoint0Label = a.FindViewById<TextView>(Resource.Id.SecondJoint0Label);
            TextView secondJoint1Label = a.FindViewById<TextView>(Resource.Id.SecondJoint1Label);
            TextView thirdJointTextLabel = a.FindViewById<TextView>(Resource.Id.ThirdJointTextLabel);
            TextView thirdJoint0Label = a.FindViewById<TextView>(Resource.Id.ThirdJoint0Label);
            TextView thirdJoint1Label = a.FindViewById<TextView>(Resource.Id.ThirdJoint1Label);
            TextView rotateTextLabel = a.FindViewById<TextView>(Resource.Id.RotateTextLabel);
            TextView rotate0Label = a.FindViewById<TextView>(Resource.Id.Rotate0Label);
            TextView rotate1Label = a.FindViewById<TextView>(Resource.Id.Rotate1Label);
            TextView grabTextLabel = a.FindViewById<TextView>(Resource.Id.GrabTextLabel);
            TextView grab0Label = a.FindViewById<TextView>(Resource.Id.Grab0Label);
            TextView grab1Label = a.FindViewById<TextView>(Resource.Id.Grab1Label);
            TextView x0Label = a.FindViewById<TextView>(Resource.Id.X0Label);
            TextView x1Label = a.FindViewById<TextView>(Resource.Id.X1Label);
            TextView y0Label = a.FindViewById<TextView>(Resource.Id.Y0Label);
            TextView y1Label = a.FindViewById<TextView>(Resource.Id.Y1Label);
            TextView z0Label = a.FindViewById<TextView>(Resource.Id.Z0Label);
            TextView z1Label = a.FindViewById<TextView>(Resource.Id.Z1Label);
            TextView completeStringTextLabel = a.FindViewById<TextView>(Resource.Id.CompleteStringTextLabel);
            TextView completeStringValueLabel = a.FindViewById<TextView>(Resource.Id.CompleteStringValueLabel);
            CheckBox deleteCheckBox = a.FindViewById<CheckBox>(Resource.Id.DeleteCheckBox);
            EditText locationNameEditText = a.FindViewById<EditText>(Resource.Id.LocationNameEditText);


            //Setting up buttons

            xPlusButton.SetX(0);
            xPlusButton.SetY(100);
            xMinusButton.SetX(200);
            xMinusButton.SetY(100);
            yPlusButton.SetX(100);
            yPlusButton.SetY(0);
            yMinusButton.SetX(100);
            yMinusButton.SetY(200);
            xyClockwiseButton.SetX(300);
            xyClockwiseButton.SetY(0);
            xyCClockwiseButton.SetX(300);
            xyCClockwiseButton.SetY(100);
            lastJointUpButton.SetX(600);
            lastJointUpButton.SetY(0);
            lastJointDownButton.SetX(600);
            lastJointDownButton.SetY(100);
            grabClockwiseButton.SetX(750);
            grabClockwiseButton.SetY(0);
            grabCClockwiseButton.SetX(750);
            grabCClockwiseButton.SetY(100);
            connectButton.SetX(750);
            connectButton.SetY(300);
            executeMacroButton.SetX(800);
            executeMacroButton.SetY(500);
            allLocationListAddButton.SetX(0);
            allLocationListAddButton.SetY(400);
            allLocationListRemoveButton.SetX(100);
            allLocationListRemoveButton.SetY(400);
            selectedLocationListAddButton.SetX(200);
            selectedLocationListAddButton.SetY(500);
            selectedLocationListRemoveButton.SetX(300);
            selectedLocationListRemoveButton.SetY(400);
            macroListAddButton.SetX(500);
            macroListAddButton.SetY(500);
            macroListRemoveButton.SetX(600);
            macroListRemoveButton.SetY(400);
            //yPlusButton.Click += delegate { xPlusButton.Text="xD"; };


            //Setting up spinners

            allLocationListSpinner.SetX(0);
            allLocationListSpinner.SetY(500);
            selectedLocationListSpinner.SetX(300);
            selectedLocationListSpinner.SetY(500);
            toMacroListSpinner.SetX(600);
            toMacroListSpinner.SetY(500);

            //Setting up labels

            baseTextLabel.Text = "Base";
            baseTextLabel.SetX(200);
            baseTextLabel.SetY(200);
            base0Label.Text = "0";
            base0Label.SetX(200);
            base0Label.SetY(230);
            base1Label.Text = "0";
            base1Label.SetX(200);
            base1Label.SetY(260);
            firstJointTextLabel.Text = "1st joint";
            firstJointTextLabel.SetX(baseTextLabel.GetX() + 100);
            firstJointTextLabel.SetY(baseTextLabel.GetY());
            firstJoint0Label.Text = "0";
            firstJoint0Label.SetX(base0Label.GetX() + 100);
            firstJoint0Label.SetY(base0Label.GetY());
            firstJoint1Label.Text = "0";
            firstJoint1Label.SetX(base1Label.GetX() + 100);
            firstJoint1Label.SetY(base1Label.GetY());
            secondJointTextLabel.Text = "2nd joint";
            secondJointTextLabel.SetX(firstJointTextLabel.GetX() + 120);
            secondJointTextLabel.SetY(firstJointTextLabel.GetY());
            secondJoint0Label.Text = "0";
            secondJoint0Label.SetX(firstJoint0Label.GetX() + 120);
            secondJoint0Label.SetY(firstJoint0Label.GetY());
            secondJoint1Label.Text = "0";
            secondJoint1Label.SetX(firstJoint1Label.GetX() + 120);
            secondJoint1Label.SetY(firstJoint1Label.GetY());
            thirdJointTextLabel.Text = "3rd joint";
            thirdJointTextLabel.SetX(secondJointTextLabel.GetX() + 130);
            thirdJointTextLabel.SetY(secondJointTextLabel.GetY());
            thirdJoint0Label.Text = "0";
            thirdJoint0Label.SetX(secondJoint0Label.GetX() + 130);
            thirdJoint0Label.SetY(secondJoint0Label.GetY());
            thirdJoint1Label.Text = "0";
            thirdJoint1Label.SetX(secondJoint1Label.GetX() + 130);
            thirdJoint1Label.SetY(secondJoint1Label.GetY());
            rotateTextLabel.Text = "rotate";
            rotateTextLabel.SetX(thirdJointTextLabel.GetX() + 130);
            rotateTextLabel.SetY(thirdJointTextLabel.GetY());
            rotate0Label.Text = "0";
            rotate0Label.SetX(thirdJoint0Label.GetX() + 130);
            rotate0Label.SetY(thirdJoint0Label.GetY());
            rotate1Label.Text = "0";
            rotate1Label.SetX(thirdJoint1Label.GetX() + 130);
            rotate1Label.SetY(thirdJoint1Label.GetY());
            grabTextLabel.Text = "grab";
            grabTextLabel.SetX(rotateTextLabel.GetX() + 100);
            grabTextLabel.SetY(rotateTextLabel.GetY());
            grab0Label.Text = "0";
            grab0Label.SetX(rotate0Label.GetX() + 100);
            grab0Label.SetY(rotate0Label.GetY());
            grab1Label.Text = "0";
            grab1Label.SetX(rotate1Label.GetX() + 100);
            grab1Label.SetY(rotate1Label.GetY());
            x0Label.Text = "0";
            x0Label.SetX(grabTextLabel.GetX() + 100);
            x0Label.SetY(grabTextLabel.GetY());
            x1Label.Text = "0";
            x1Label.SetX(x0Label.GetX() + 90);
            x1Label.SetY(x0Label.GetY());
            y0Label.Text = "0";
            y0Label.SetX(grab0Label.GetX() + 100);
            y0Label.SetY(grab0Label.GetY());
            y1Label.Text = "0";
            y1Label.SetX(y0Label.GetX() + 90);
            y1Label.SetY(y0Label.GetY());
            z0Label.Text = "0";
            z0Label.SetX(grab1Label.GetX() + 100);
            z0Label.SetY(grab1Label.GetY());
            z1Label.Text = "0";
            z1Label.SetX(z0Label.GetX() + 90);
            z1Label.SetY(z0Label.GetY());

            deleteCheckBox.SetX(0);
            deleteCheckBox.SetY(300);
            locationNameEditText.SetX(300);
            locationNameEditText.SetY(300);
        }
     

    }
}