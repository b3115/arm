﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARM2
{
    class macro:List<Location>
    {
        public string name;

        public macro(string name)
        {
            this.name = name;
        }
        
        public macro(List<Location> collection, string name)
        {
            this.name = name;
            this.AddRange(collection);
        }
    }
}
