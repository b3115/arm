﻿using System;
using System.Text;

namespace ARM2
{
    class Location
    {
        public Location(string name, double alpha, double beta, double gamma, double delta, double rot, double grab)
        {
            this.alpha = alpha;
            this.rot = rot;
            this.grab = grab;
            this.beta = beta;
            this.gamma = gamma;
            this.delta = delta;
            this.name = name;
        }
        public string name;
        public double alpha;
        //variables describing the grabber
        public double rot;
        public double grab;
        //angles from inverse kinematics
        public double beta; // 1st joint
        public double gamma; // 2nd joint
        public double delta; // 3rd joint

        public void edit(string name, double alpha, double beta, double gamma, double delta, double rot, double grab)
        {
            this.alpha = alpha;
            this.rot = rot;
            this.grab = grab;
            this.beta = beta;
            this.gamma = gamma;
            this.delta = delta;
            this.name = name;
        }

        public override String ToString()
        {
            StringBuilder result = new StringBuilder();
            if (alpha >= 0) result.Append("0");
            else result.Append("1");
            result.Append(Math.Abs(alpha).ToString().PadLeft(3, '0'));
            if (beta >= 0) result.Append(" 0");
            else result.Append(" 1");
            result.Append(Math.Abs(Math.Round(beta, 0)).ToString().PadLeft(3, '0'));
            if (gamma >= 0) result.Append(" 0");
            else result.Append(" 1");
            result.Append(Math.Abs(Math.Round(gamma, 0)).ToString().PadLeft(3, '0'));
            if (delta >= 0) result.Append(" 0");
            else result.Append(" 1");
            result.Append(Math.Abs(Math.Round(delta, 0)).ToString().PadLeft(3, '0'));
            if (rot >= 0) result.Append(" 0");
            else result.Append(" 1");
            result.Append(Math.Abs(rot).ToString().PadLeft(3, '0'));
            if (grab >= 0) result.Append(" 0");
            else result.Append(" 1");
            result.Append(Math.Abs(grab).ToString().PadLeft(3, '0'));
            return result.ToString();
        }

        

    }
}
